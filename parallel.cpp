#include <iostream>
using namespace std;

/* Application to calculate a rectangular parallelepiped

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 30 / 04 / 2022
   License  => Cardware
   
*/


int main ( )
{

float side1;
float side2;
float side3;
float surface1;
float volume1;

	cout << "Insert the side1 of the rectangular parallelepiped : \n";
	cin  >> side1;
	cout << " \n";
	
	cout << "Insert the side2 of the rectangular parallelepiped : \n";
	cin  >> side2;
	cout << " \n";
	
	cout << "Insert the side3 of the rectangular parallelepiped : \n";
	cin  >> side3;
	cout << " \n";
	
	surface1  = side1 * side2 + side2 * side3 + side2 * side3;
	volume1    = side1 * side2 * side3;
	
	cout << "The surface area is :  ";
	cout << surface1;
	cout << " \n";
	
	cout << "The volume is :  ";
	cout << volume1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	return 0;
	
}
	
