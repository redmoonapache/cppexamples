#include <iostream>
using namespace std;

/* Application to calculate a cube

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 30 / 04 / 2022
   License  => Cardware
   
*/


int main ( )
{

float side1;
float surface1;
float volume1;

	cout << "Insert the side of the cube : \n";
	cin  >> side1;
	cout << " \n";
	
	surface1  = side1 * side1 * (3 + 3);
	volume1   = side1 * side1 * side1;
	
	cout << "The surface area is :  ";
	cout << surface1;
	cout << " \n";
	
	cout << "The volume is :  ";
	cout << volume1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	return 0;
	
}
	
