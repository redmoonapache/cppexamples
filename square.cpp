#include <iostream>
using namespace std;

/* Application to calculate a square

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 29 / 04 / 2022
   License  => Cardware
   
*/


int main ( )
{

float side1;
float area1;
float perim1;

	cout << "Insert the side of the square : \n";
	cin  >> side1;
	cout << " \n";
	
	perim1 = 4 * side1;
	area1  = side1 * side1;
	
	cout << "The perimeter is :  ";
	cout << perim1;
	cout << " \n";
	
	cout << "The area is :  ";
	cout << area1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	return 0;
	
}
	
