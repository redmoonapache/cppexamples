#include <iostream>
using namespace std;

/* Application to calculate a circle

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 29 / 04 / 2022
   License  => Cardware
   
*/


int main ( )
{

float radius1;
float pigr;
float area1;
float perim1;

	pigr = 3.14159;	

	cout << "Insert the radius of the circle : \n";
	cin  >> radius1;
	cout << " \n";
	
	perim1 = 2 * pigr * radius1;
	area1  = pigr * radius1 * radius1;
	
	cout << "The perimeter is :  ";
	cout << perim1;
	cout << " \n";
	
	cout << "The area is :  ";
	cout << area1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	
	return 0;
	
}
	
