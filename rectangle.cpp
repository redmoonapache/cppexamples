#include <iostream>
using namespace std;

/* Application to calculate a rectangle

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 29 / 04 / 2022
   License  => Cardware
   
*/


int main ( )
{

float side1;
float side2;
float area1;
float perim1;

	cout << "Insert the side 1 of the rectangle : \n";
	cin  >> side1;
	cout << " \n";
	
	cout << "Insert the side 2 of the rectangle : \n";
	cin  >> side2 ;
	cout << " \n";
	
	perim1 = 2 * side1 + 2 * side2;
	area1  = side1 * side2;
	
	cout << "The perimeter is :  ";
	cout << perim1;
	cout << " \n";
	
	cout << "The area is :  ";
	cout << area1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	
	return 0;
	
}
	
