#include <iostream>
using namespace std;

/* Application to calculate a triangle

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 29 / 04 / 2022
   License  => Cardware
   
*/


int main ( )
{

float base1;
float altezza1;
float area1;

	cout << "Insert the base of the triangle : \n";
	cin  >> base1;
	cout << " \n";
	
	cout << "Insert the height of the triangle : \n";
	cin  >> altezza1 ;
	cout << " \n";
	
	area1  = base1 * altezza1 / 2;
	
	cout << "The area is :  ";
	cout << area1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	
	return 0;
	
}
	
