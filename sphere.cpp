#include <iostream>
using namespace std;

/* Application to calculate a sphere

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 30 / 04 / 2022
   License  => Cardware
   
*/


int main ( )
{

float radius1;
float surface1;
float volume1;
float pigr;

	pigr = 3.14159;

	cout << "Insert the radius of the sphere : \n";
	cin  >> radius1;
	cout << " \n";
	
	surface1 = 4 * pigr * radius1 * radius1;
	volume1  = 4 * pigr * radius1 * radius1 * radius1 / 3;
	
	cout << "The surface area is :  ";
	cout << surface1;
	cout << " \n";
	
	cout << "The volume is :  ";
	cout << volume1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	return 0;
	
}
	
