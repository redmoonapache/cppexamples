#include <iostream>
using namespace std;

/* Application to calculate a pentagon

   Author   => Mattia Tristo
   EMail    => mattia.tristo@aol.com
   Date     => 02 / 05 / 2022
   License  => Cardware
   
*/


int main ( )
{

float side1;
float const1;
float area1;
float perim1;

	const1 = 1.72047;

	cout << "Insert the side of the regular pentagon : \n";
	cin  >> side1;
	cout << " \n";
	
	perim1 = 5 * side1;
	area1  = const1 * side1 * side1;
	
	cout << "The perimeter is :  ";
	cout << perim1;
	cout << " \n";
	
	cout << "The area is :  ";
	cout << area1;
	cout << "\n";
	
	cout << "Done. \n";
	cout << "\n";
	
	return 0;
	
}
	
